package com.hanter.free.mvppatterndemo.test;


import com.hanter.free.mvppatterndemo.bean.Article;
import com.hanter.free.mvppatterndemo.mvp.BasePresenter;
import com.hanter.free.mvppatterndemo.mvp.BaseView;

import java.util.List;

public interface TestContract {

    interface Presenter extends BasePresenter<View> {
        void fetchArticles();
    }

    interface View extends BaseView {
        void showLoading();
        void hiddenLoading();

        void showArticles(List<Article> articles);
    }

}
