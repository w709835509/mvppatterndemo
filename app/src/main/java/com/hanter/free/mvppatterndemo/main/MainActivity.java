package com.hanter.free.mvppatterndemo.main;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hanter.free.mvppatterndemo.R;
import com.hanter.free.mvppatterndemo.bean.Article;
import com.hanter.free.mvppatterndemo.mvp.MVPBaseActivity;

import java.util.List;

public class MainActivity extends MVPBaseActivity<ArticleContract.View, MainPresenter> implements ArticleContract.View, View.OnClickListener {

    private ArticleListAdapter mArticleListAdapter;
    private ListView mArticleList;

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mArticleListAdapter = new ArticleListAdapter(this);
        mArticleList = (ListView) findViewById(R.id.lv_main_content);
        mArticleList.setAdapter(mArticleListAdapter);
    }

    @Override
    public void showLoading() {
        Toast.makeText(MainActivity.this, "正在加载中", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hiddenLoading() {
        Toast.makeText(MainActivity.this, "加载完成", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showArticles(List<Article> articles) {
        mArticleListAdapter.setArticles(articles);
        mArticleListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_main_load:
                mPresenter.fetchArticles();
                break;
        }
    }

    @Override
    public Context getContext() {
        return this;
    }


    static class ArticleListAdapter extends BaseAdapter {

        private Context mContext;
        private List<Article> mArticles;

        public ArticleListAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            if (mArticles != null) {
                return mArticles.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int position) {
            if (mArticles != null && position < mArticles.size() && position >= 0) {
                return mArticles.get(position);
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();

                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_data, parent, false);

                holder.content = (TextView) convertView.findViewById(R.id.tv_item_data_content);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Article article = (Article) getItem(position);

            holder.content.setText(article.getName());

            return convertView;
        }

        public void setArticles(List<Article> articles) {
            mArticles = articles;
        }

        static class ViewHolder {
            TextView content;
        }
    }
}
