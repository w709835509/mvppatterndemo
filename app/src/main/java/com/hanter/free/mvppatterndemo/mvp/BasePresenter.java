package com.hanter.free.mvppatterndemo.mvp;

public interface BasePresenter<T extends BaseView> {

    void attachView(T view);

    T getView();

    boolean isViewAttached();

    void detachView();
}
