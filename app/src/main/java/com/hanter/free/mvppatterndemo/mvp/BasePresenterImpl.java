package com.hanter.free.mvppatterndemo.mvp;

import java.lang.ref.WeakReference;

/**
 * 类名：BasePresenterImpl <br/>
 * 描述：实现BasePresenter的基类
 * 创建时间：2016/03/17 18:42
 *
 * @author hanter
 * @version 1.0
 */
public abstract class BasePresenterImpl<T extends BaseView> implements BasePresenter<T> {

    private WeakReference<T> mViewRef;

    @Override
    public void attachView(T view) {
        mViewRef = new WeakReference<>(view);
    }

    @Override
    public T getView() {
        return mViewRef.get();
    }

    @Override
    public boolean isViewAttached() {
        return mViewRef != null && mViewRef.get() != null;
    }

    @Override
    public void detachView() {
        if (mViewRef != null) {
            mViewRef.clear();
            mViewRef = null;
        }
    }
}
