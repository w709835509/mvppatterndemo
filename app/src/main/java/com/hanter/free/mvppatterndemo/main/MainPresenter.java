package com.hanter.free.mvppatterndemo.main;

import android.os.Handler;
import android.os.Message;

import com.hanter.free.mvppatterndemo.mvp.BasePresenterImpl;
import com.hanter.free.mvppatterndemo.model.ArticleModel;
import com.hanter.free.mvppatterndemo.model.DataListener;
import com.hanter.free.mvppatterndemo.bean.Article;
import com.hanter.free.mvppatterndemo.model.imp.ArticleModelImpl;

import java.util.List;

/**
 * 类名：MainPresenter <br/>
 * 描述：主Presenter
 * 创建时间：2016/03/16 20:12
 *
 * @author hanter
 * @version 1.0
 */
public class MainPresenter extends BasePresenterImpl<ArticleContract.View> implements ArticleContract.Presenter {

    ArticleModel mArticleModel = new ArticleModelImpl();

    public MainPresenter() {

    }

    @Override
    public void fetchArticles() {

        getView().showLoading();

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mArticleModel.loadArticlesFromCache(new DataListener<List<Article>>() {
                    @Override
                    public void onComplete(List<Article> data) {
                        getView().showArticles(data);

                        getView().hiddenLoading();
                    }
                });
            }
        };


        handler.sendEmptyMessageDelayed(0, 2000);
    }


}
