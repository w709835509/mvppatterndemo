package com.hanter.free.mvppatterndemo.model.imp;

import com.hanter.free.mvppatterndemo.model.ArticleModel;
import com.hanter.free.mvppatterndemo.model.DataListener;
import com.hanter.free.mvppatterndemo.bean.Article;

import java.util.ArrayList;
import java.util.List;

/**
 * 类名：ArticleModelImpl <br/>
 * 描述：TODO
 * 创建时间：2016/03/17 15:16
 *
 * @author hanter
 * @version 1.0
 */
public class ArticleModelImpl implements ArticleModel {
    List<Article> mArticles = new ArrayList<Article>();

    @Override
    public void loadArticlesFromCache(DataListener<List<Article>> listener) {
        mArticles.add(new Article("test", "test"));
        mArticles.add(new Article("test", "test"));
        mArticles.add(new Article("test", "test"));
        mArticles.add(new Article("test", "test"));
        listener.onComplete(mArticles);
    }
}
