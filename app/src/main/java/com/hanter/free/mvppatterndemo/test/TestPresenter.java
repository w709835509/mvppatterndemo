package com.hanter.free.mvppatterndemo.test;

import android.os.Handler;
import android.os.Message;

import com.hanter.free.mvppatterndemo.mvp.BasePresenterImpl;
import com.hanter.free.mvppatterndemo.model.ArticleModel;
import com.hanter.free.mvppatterndemo.model.DataListener;
import com.hanter.free.mvppatterndemo.bean.Article;
import com.hanter.free.mvppatterndemo.model.imp.ArticleModelImpl;

import java.util.List;

/**
 * 名称: TestPresenter <br/>
 * 描述: TODO <br/>
 * 创建时间：2016/3/18 9:50
 *
 * @author wangmingshuo@ddsoucai.cn
 * @version 1.0
 */
public class TestPresenter extends BasePresenterImpl<TestContract.View> implements TestContract.Presenter {
    ArticleModel mArticleModel = new ArticleModelImpl();

    public TestPresenter() {

    }

    @Override
    public void fetchArticles() {

        getView().showLoading();

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mArticleModel.loadArticlesFromCache(new DataListener<List<Article>>() {
                    @Override
                    public void onComplete(List<Article> data) {
                        getView().showArticles(data);

                        getView().hiddenLoading();
                    }
                });
            }
        };


        handler.sendEmptyMessageDelayed(0, 2000);
    }
}
