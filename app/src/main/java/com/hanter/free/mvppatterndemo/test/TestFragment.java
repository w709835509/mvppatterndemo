package com.hanter.free.mvppatterndemo.test;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hanter.free.mvppatterndemo.mvp.MVPBaseFragment;
import com.hanter.free.mvppatterndemo.R;
import com.hanter.free.mvppatterndemo.bean.Article;

import java.util.List;

/**
 * 名称: TestFragment <br/>
 * 描述: TODO <br/>
 * 创建时间：2016/3/18 9:49
 *
 * @author wangmingshuo@ddsoucai.cn
 * @version 1.0
 */
public class TestFragment extends MVPBaseFragment<TestContract.View, TestPresenter> implements TestContract.View, View.OnClickListener {

    private Button btnTest;
    private ArticleListAdapter mArticleListAdapter;
    private ListView mArticleList;

    @Override
    protected TestPresenter createPresenter() {
        return new TestPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        btnTest = (Button) rootView.findViewById(R.id.btn_fragment_test_load);
        btnTest.setOnClickListener(this);
        mArticleListAdapter = new ArticleListAdapter(getContext());
        mArticleList = (ListView) rootView.findViewById(R.id.lv_fragment_test_content);
        mArticleList.setAdapter(mArticleListAdapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void showLoading() {
        Toast.makeText(getContext(), "正在加载中", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hiddenLoading() {
        Toast.makeText(getContext(), "加载完成", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showArticles(List<Article> articles) {
        mArticleListAdapter.setArticles(articles);
        mArticleListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_fragment_test_load:
                mPresenter.fetchArticles();
                break;
        }
    }

    static class ArticleListAdapter extends BaseAdapter {

        private Context mContext;
        private List<Article> mArticles;

        public ArticleListAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            if (mArticles != null) {
                return mArticles.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int position) {
            if (mArticles != null && position < mArticles.size() && position >= 0) {
                return mArticles.get(position);
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();

                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_data, parent, false);

                holder.content = (TextView) convertView.findViewById(R.id.tv_item_data_content);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Article article = (Article) getItem(position);

            holder.content.setText(article.getName());

            return convertView;
        }

        public void setArticles(List<Article> articles) {
            mArticles = articles;
        }

        static class ViewHolder {
            TextView content;
        }
    }
}
