package com.hanter.free.mvppatterndemo.model;

/**
 * 类名：DataListener <br/>
 * 描述：数据加载回调
 * 创建时间：2016/03/17 15:12
 *
 * @author hanter
 * @version 1.0
 */
public interface DataListener<T> {
    void onComplete(T data);
}
