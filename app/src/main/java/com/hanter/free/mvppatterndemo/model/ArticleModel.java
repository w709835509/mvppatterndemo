package com.hanter.free.mvppatterndemo.model;

import com.hanter.free.mvppatterndemo.bean.Article;

import java.util.List;

/**
 * 类名：ArticleModel <br/>
 * 描述：TODO
 * 创建时间：2016/03/16 20:16
 *
 * @author hanter
 * @version 1.0
 */
public interface ArticleModel {
    void loadArticlesFromCache(DataListener<List<Article>> listener);
}
