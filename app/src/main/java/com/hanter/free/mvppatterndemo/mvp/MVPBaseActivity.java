package com.hanter.free.mvppatterndemo.mvp;

import android.os.Bundle;

import com.hanter.free.mvppatterndemo.BaseActivity;

/**
 * 类名：MVPBaseActivity <br/>
 * 描述：MVP结构的Activity
 * 创建时间：2016/03/17 18:45
 *
 * @author hanter
 * @version 1.0
 */
public abstract class MVPBaseActivity<V extends BaseView, T extends BasePresenterImpl<V>> extends BaseActivity {
    protected T mPresenter;

    protected abstract T createPresenter();

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = createPresenter();
        mPresenter.attachView((V) this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }
}
