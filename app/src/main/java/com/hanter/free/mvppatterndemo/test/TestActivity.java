package com.hanter.free.mvppatterndemo.test;

import android.os.Bundle;

import com.hanter.free.mvppatterndemo.BaseActivity;
import com.hanter.free.mvppatterndemo.R;

public class TestActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
