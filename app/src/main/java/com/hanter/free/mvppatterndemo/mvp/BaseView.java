package com.hanter.free.mvppatterndemo.mvp;


import android.content.Context;

public interface BaseView {
    Context getContext();
}
