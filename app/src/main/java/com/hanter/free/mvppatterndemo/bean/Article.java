package com.hanter.free.mvppatterndemo.bean;

/**
 * 类名：Article <br/>
 * 描述：文章实体类
 * 创建时间：2016/03/16 20:15
 *
 * @author hanter
 * @version 1.0
 */
public class Article {
    private String name;
    private String content;

    public Article() {

    }

    public Article(String name, String content) {
        this.name = name;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
